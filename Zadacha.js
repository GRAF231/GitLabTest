function reversePrint (linkedList) {
    var N = linkedList;
    var arr = [];
    while(N.next !== null)
    {
        arr.unshift(N.value);
        N = N.next;

    }
    arr.unshift(N.value);

    console.log(arr);
}

var someList = {
    value: 1,
    next: {
        value: 2,
        next: {
            value: 3,
            next: {
                value: 4,
                next: null
            }
        }
    }
};
reversePrint(someList);
